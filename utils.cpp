#include "utils.hpp"


bool Utils::isAlpha(char c) { 
    return ('A'<=c && 'Z'>=c) || ('a'<=c && 'z'>=c);
}

bool Utils::isNumeric(char c) {
    return (c>='0') && (c<='9');
}

