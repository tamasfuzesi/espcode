#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include "utils.hpp"

typedef std::string string;

enum TokenType {
    KEYWORD,
    NUMBER,
    PARAM,
    BOOLEAN,
    COLON,
    SEMICOLON,
    COMMA,
    STRING,
    LPAREN,
    RPAREN,
    MINUS,
    PLUS,
    MUL, // not implemented
    DIV, // not implemented
    GT, // not implemented
    GTE, // not implemented
    LT, // not implemented
    LTE // not implemented
};
std::map<TokenType, string> tokenTypeMap = {
    {TokenType::KEYWORD, "KEYWORD"},
    {TokenType::NUMBER, "NUMBER"},
    {TokenType::PARAM, "PARAM"},
    {TokenType::BOOLEAN, "BOOLEAN"},
    {TokenType::COLON, "COLON"},
    {TokenType::SEMICOLON, "SEMICOLON"},
    {TokenType::COMMA, "COMMA"},
    {TokenType::STRING, "STRING"},
    {TokenType::RPAREN, "RPAREN"},
    {TokenType::LPAREN, "LPAREN"},
    {TokenType::MINUS, "MINUS"},
    {TokenType::PLUS, "PLUS"},
    {TokenType::MUL, "MUL"}, // not implemented
    {TokenType::DIV, "DIV"}, // not implemented
    {TokenType::GT, "GT"}, // not implemented
    {TokenType::GTE, "GTE"}, // not implemented
    {TokenType::LT, "LT"}, // not implemented
    {TokenType::LTE, "LTE"}, // not implemented
};
string keywords[] = {"PIN", "DISPLAY", "TRUE", "FALSE", "DELAY"};
string literals[] = {"NUMBER", "BOOLEAN", "STRING"};
string operators[] = {"PLUS", "MINUS", "MUL", "DIV", "GT", "GTE", "LT", "LTE"};
string commands[] = {"PIN", "DISPLAY", "DELAY"};
string paramTypes[] = {"NUMBER", "STRING", "BOOLEAN"};

class Exception{
private:
    int code;
    string message;
    int line;
public:
    Exception(int code, int line, string message){
        this->code = code;
        this->message = message;
        this->line = line;
    }
    Exception(int code, string message){
        Exception(code, 0, message);
    }
    string getMessage(){
        if(this->line > 0){
            return message + " at line " + std::to_string(line);
        }
        return message;
    }
    int getCode(){
        return code;
    }
};

class Token {
private:
    TokenType type;
    string value;
    
public:
    Token(const TokenType type, const string value) {
        this->type = type;
        this->value = value;
    };
    ~Token(){};
    string getValue() {
        return this->value;
    }
    TokenType getType(){
        return this->type;
    }
    bool isCommand(){
        // FIXME: optimize
        for(string command: commands){
            if(command == getValue()){
                return true;
            }
        }
        return false;
    }
    string toString() {
        return "( " + tokenTypeMap[this->type] + ": " + this->value + " )";
    }
    bool toBoolean() {
        if(this->type == TokenType::BOOLEAN){
            return this->value == "TRUE";
        }
        throw Exception(303, "Invalid TokenType");
    }
};

class Lexer {
private:
    int position = -1;
    unsigned int line = 1;
    string code;
    char currentChar;
    std::vector<Token> tokens;
    
public:
    Lexer(string text) {
        code = text;
    };
    
    void advance() {
        ++position;
        currentChar = code[position];
    }
    
    void makeTokens() {
        advance();
        // FIXME: change to switch-case
        while (currentChar != '\0') {
            if (currentChar == '\t' || currentChar == ' ') {
                advance();
            } else if(currentChar == '\n'){
                ++line;
                advance();
            }
            else if (currentChar == ';') {
                tokens.push_back(Token(TokenType::SEMICOLON, string(1, currentChar)));
                advance();
            } else if (currentChar == ':') {
                tokens.push_back(Token(TokenType::COLON, string(1, currentChar)));
                advance();
            } else if (currentChar == ',') {
                tokens.push_back(Token(TokenType::COMMA, string(1, currentChar)));
                advance();
            } else if (currentChar == '-') {
                tokens.push_back(Token(TokenType::MINUS, string(1, currentChar)));
                advance();
            } else if (currentChar == '+') {
                tokens.push_back(Token(TokenType::PLUS, string(1, currentChar)));
                advance();
            } else if (currentChar == '(') {
                tokens.push_back(Token(TokenType::LPAREN, string(1, currentChar)));
                advance();
            } else if (currentChar == ')') {
                tokens.push_back(Token(TokenType::RPAREN, string(1, currentChar)));
                advance();
            } else if (currentChar == '"') {
                tokens.push_back(Token(TokenType::STRING, makeString()));
                advance();
            } else if (Utils::isAlpha(currentChar)) {
                tokens.push_back(makeKeywordToken());
            } else if (Utils::isNumeric(currentChar)) {
                tokens.push_back(Token(TokenType::NUMBER, makeNumber()));
            }
            
            else {
                throw Exception(100, line, "Unexpected token: " + std::to_string(currentChar));
                break;
            }
        }
        
        for (Token token : tokens) {
            std::cout << token.toString() << std::endl;
        }
    };
    
    string makeString() {
        string s;
        
        advance();
        
        while (currentChar != '"' && position < code.length()) {
            s.push_back(currentChar);
            advance();
        }
        
        return s;
    };
    
    Token makeKeywordToken() {
        string s;
        
        while(Utils::isAlpha(currentChar)){
            s.push_back(toupper(currentChar));
            advance();
        }
        
        if(! (std::find(std::begin(keywords), std::end(keywords), s) != std::end(keywords))){
            throw Exception(102, line, "Invalid keyword: " + s);
        }

        if(s == "TRUE" || s == "FALSE"){
            return Token(TokenType::BOOLEAN, s);
        }
        
        return Token(TokenType::KEYWORD, s);
    }
    
    string makeNumber() {
        string s;
        
        while(Utils::isNumeric(currentChar) || currentChar == '.'){
            s.push_back(currentChar);
            advance();
        }
        
        return s;
    }
    
    std::vector<Token> getTokens(){
        return tokens;
    }
    
    ~Lexer(){};
};


class Command{
protected:
    std::vector<string> params;
public:
    Command(){};
    virtual void execute(){
        std::cout << "execute" << std::endl;
    };
    Command(std::vector<string> params){
        this->params = params;
    }
    void addParam(string param){
        this->params.push_back(param);
    }
    void setParams(std::vector<string> params){
        this->params = params;
    }
    std::vector<string> getParams(){
        return this->params;
    }
};


class CommandProvider{
private:
    std::map<string, Command*> commands;
public:
    CommandProvider(){}
    void addCommand(string commandName, Command *command){
        this->commands.insert({commandName, command});
    }
    Command* getCommand(string command){
        if(! (this->commands.find(command) == this->commands.end())){
            return this->commands[command];
        }
        throw Exception(304, "Command is not implemented");
    }
};

enum NodeType {
    None,
    Command
};

class Node{
public:
    virtual NodeType getType(){
        return NodeType::None;
    };
};

class CommandNode: public Node{
private:
    string command;
    std::vector<string> params;
    NodeType type;
    
public:
    CommandNode(string command, std::vector<string> params){
        this->type = NodeType::Command;
        this->command = command;
        this->params = params;
    }
    string getCommand(){
        return this->command;
    }
    std::vector<string> getParams() {
        return this->params;
    }
    NodeType getType(){
        return this->type;
    }
};


class Parser {
private:
    std::vector<Token> tokens;
    Token currentToken;
    std::vector<Node*> nodes;
    int position = 0;
public:
    Parser(std::vector<Token> tokens): currentToken(tokens.at(0)){
        this->tokens = tokens;
    }
    void advance(){
        ++this->position;
        this->currentToken = this->tokens.at(this->position);
    }
    std::vector<Node*> parse(){
        while(this->position < this->tokens.size() -1){
            if(currentToken.getType() == TokenType::KEYWORD && currentToken.isCommand()){
                pushCommandNode();
            }
            
            this->advance();
        }
        
        return this->nodes;
    }
    std::vector<Node*> getNodes(){
        return this->nodes;
    }
    void pushCommandNode(){
        string command = currentToken.getValue();
        std::vector<string> params;
        advance();
        while(currentToken.getType() != TokenType::SEMICOLON){
            if(currentToken.getType() != TokenType::NUMBER
                && currentToken.getType() != TokenType::STRING
                && currentToken.getType() != TokenType::BOOLEAN){
                    advance();
                    continue;
                }
            params.push_back(currentToken.getValue());
            advance();
        }
        
        this->nodes.push_back(new CommandNode(command, params));
    }
    
};

class Interpreter {
    std::vector<Node*> nodes;
    CommandProvider commandProvider;
public:
    Interpreter(std::vector<Node*> nodes, CommandProvider &commandProvider) {
        this->nodes = nodes;
        this->commandProvider = commandProvider;
    }
    
    void run(){
        std::cout << "Nodes size: " << this->nodes.size() << std::endl;
        for (int i =0; i < this->nodes.size(); i++) {
            this->solveNode(this->nodes[i]);
        }
    }
    
    void solveNode(Node *&node) {
        switch (node->getType()) {
            case NodeType::Command: {
                CommandNode* commandNode = dynamic_cast<CommandNode*>(node);
                this->solveCommandNode(commandNode);
                break;
            }
            default:
                throw Exception(302, "Invalid NodeType");
                
        }
    }
    
    void solveCommandNode(CommandNode *commandNode){
        auto command = this->commandProvider.getCommand(commandNode->getCommand());
        command->setParams(commandNode->getParams());
        command->execute();
    }
};
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


class PinCommand: public Command{
public:
    PinCommand(){}
    void execute() override{
        std::cout << std::endl << "execute PinCommand" << std::endl;
        for(string param: this->getParams()){
            std::cout << param << " ";
            
        }
    }
};

class DisplayCommand: public Command{
public:
    void execute() override{
        std::cout << std::endl << std::endl << "execute DisplayCommand" << std::endl;
        for(string param: this->getParams()){
            std::cout << param << " ";
        }
    }
};

class DelayCommand: public Command{
public:
    void execute() override{
        std::cout << std::endl << std::endl << "execute DelayCommand" << std::endl;
        for(string param: this->getParams()){
            std::cout << param << " ";
        }
    }
};

int main(int argc, char *argv[]) {
    try{
        CommandProvider commandProvider;
        commandProvider.addCommand("PIN", new PinCommand());
        commandProvider.addCommand("DISPLAY", new DisplayCommand());
        commandProvider.addCommand("DELAY", new DelayCommand());
        
        Lexer lexer("PIN: 1, TRUE; DISPLAY: \"Hellóka\"; \n"
                    "(+0.25)\n"
                    "DELAY: 1500;"
                    "PIN: 2, FALSE;"
                    "\"\"");
        lexer.makeTokens();

        //std::cout << lexer.getTokens().size();
        
        Parser parser(lexer.getTokens());
        std::vector<Node*> tree = parser.parse();
        
        Interpreter interpreter(tree, commandProvider);
        interpreter.run();
        
        
    } catch(Exception exception){
        std::cerr << exception.getMessage();
    }
    

    return 0;
}
