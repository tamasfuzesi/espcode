#ifndef utils_hpp
#define utils_hpp

class Utils{
public:
    static bool isAlpha(char c);
    static bool isNumeric(char c);
};

#endif /* utils_hpp */
